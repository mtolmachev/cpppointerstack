
#include <iostream>
#include <stack>

using namespace std;

class Stack {
private:

	int NumArray = 0;
	int* StackArray = new int[NumArray];

public:
	Stack() {}
	~Stack() {
		delete[] StackArray;
	}

	void Push(int Value) {
		int* NewArray = new int[NumArray + 1];

		for (int i = 0; i < NumArray; i++) {
			NewArray[i] = StackArray[i];
		}

		NewArray[NumArray] = Value;
		delete[] StackArray;
		NumArray++;
		StackArray = NewArray;
	}

	void Print() {
		for (int i = 0; i < NumArray; i++) {
			cout << StackArray[i] << '\n';
		}
		cout << "---------------------------------------" << '\n';
	}

	void Pop() {
		NumArray--;
	}
};



int main()
{
	Stack s;
	s.Push(1);
	s.Push(10);
	s.Push(20);
	s.Push(100);
	s.Push(200);
	s.Push(48);
	s.Push(13);
	s.Push(21);
	s.Print();
	s.Pop();
	s.Print();
}